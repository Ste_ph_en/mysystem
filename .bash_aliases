alias matlab='/usr/local/MATLAB/R2018a/bin/matlab'
alias open='xdg-open ./'
alias del='gvfs-trash'
alias trash-view='gvfs-ls trash://'
alias trash-empty='gvfs-trash --empty'
alias basepy='conda activate base'
alias emacs="emacs -nw"