#import bpy
#import bmesh
import numpy as np

def sphere(shape, radius, position):
    # assume shape and position are both a 3-tuple of int or float
    # the units are pixels / voxels (px for short)
    # radius is a int or float in px
    semisizes = (radius,) * 3

    # genereate the grid for the support points
    # centered at the position indicated by position
    grid = [slice(-x0, dim - x0) for x0, dim in zip(position, shape)]
    position = np.ogrid[grid]
    # calculate the distance of all points from `position` center
    # scaled by the radius
    arr = np.zeros(shape, dtype=float)
    for x_i, semisize in zip(position, semisizes):
        arr += (np.abs(x_i / semisize) ** 2)
    # the inner part of the sphere will have distance below 1
    return arr <= 1.0

def AddBox(x,y,z,c=1):
    bpy.ops.mesh.primitive_cube_add(radius=.5, view_align=False, enter_editmode=False, location=(x, y, z))
 

if __name__ == "__main__":
    arr = sphere((3, 3, 3), 1, (1, 1, 1))
    arr = sphere((256, 256, 256), 10, (127, 127, 127))
    # this will save a sphere in a boolean array
    # the shape of the containing array is: (256, 256, 256)
    # the position of the center is: (127, 127, 127)
    # if you want is 0 and 1 just use .astype(int)
    # for plotting it is likely that you want that
    #triplet = arr.reshape(-1, 3)
    #for cor in triplet.astype(int):
    #    print(*cor)
    #    AddBox(*cor)
    arr.shape[0]
    print(arr.shape)
    # (2,3,4)
    # 3-D array
    #   along x-axis = 2 elements each
    #   along y-axis = 3 elements each
    #   along z-axis = 4 elements each
    #So, If i want to look at elements along z-axis for all x-axis and y-axis it will look like

    for x, xid in enumerate(range(arr.shape[0])):     # for each x-axis
        for y, yid in enumerate(range(arr.shape[1])): # for each y-axis
            for z, zid in enumerate(range(arr.shape[2])):
                value = (arr[xid, yid, zid].astype(int))
                #AddBox(x,y,z)     # All elements in z-axis