import random
import string
from pynput.keyboard import Key, Controller
import time

# wait to move curser
time.sleep(5)


def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


keyboard = Controller()

for _ in range(1000000):
    keyboard.type(str(id_generator(8)))
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)
    time.sleep(.3)
