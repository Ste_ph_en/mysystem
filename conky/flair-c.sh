#!/bin/bash
#This Script is written by Umair - Copyright© http://www.NoobsLab.com
#To setup conky

echo "
 -------------------------
 |N o o b s L a b . c o m|
 -------------------------
"
killall conky 2> /dev/null
if [ -d "$HOME/.conky" ]; then
	rm -r $HOME/.conky 2> /dev/null
elif [ -d "$HOME/.start-conky" ]; then
	rm $HOME/.start-conky 2> /dev/null
elif [ -f "$HOME/.config/autostart/start-conky.desktop" ]; then
	rm $HOME/.config/autostart/start-conky.desktop 2> /dev/null
elif [ -f "$HOME/.config/autostart/conky*" ]; then
	rm $HOME/.config/autostart/conky* 2> /dev/null
fi

echo "Flair conky is available in two versions Dark and Light."
echo ""
echo "Select conky and enter number....."
echo "Enter 1 for Dark Version."
echo "Enter 2 for Light Version."
echo "Type: "
read InputConky

if [ $InputConky = "1" ]; then
	{
	  echo "Downloading and installing selected conky..."
	  sleep 2
	  cd && wget -O flair-dark-conky.zip http://drive.noobslab.com/data/conky/flair/flair-dark-conky.zip
	  unzip -o flair-dark-conky.zip -d $HOME/
	  rm flair-dark-conky.zip
	}
elif [ $InputConky = "2" ]; then
	{
	  echo "Downloading and installing selected conky..."
	  sleep 2
	  cd && wget -O flair-light-conky.zip http://drive.noobslab.com/data/conky/flair/flair-light-conky.zip
	  unzip -o flair-light-conky.zip -d $HOME/
	  rm flair-light-conky.zip
	}
else
	echo "Input is invalid!!!"
fi

echo ""
echo "Checking if fonts are installed."
echo "."
sleep 1
echo ".."
sleep 1
echo "..."
sleep 1
if [[ -f "$HOME/.fonts/Dingytwo.ttf" && -f "$HOME/.fonts/GeosansLight.ttf" ]]; then
	echo "Yes, fonts are installed"
else
	echo "Fonts doesn't exist for this conky."
	echo "Downloading Fonts for conky. Connecting to NoobsLab drive..."
	sleep 2
	cd && wget -O cfonts.zip http://drive.noobslab.com/data/conky/flair/flair-conky-fonts.zip
	unzip -o cfonts.zip -d $HOME/.fonts/
	rm cfonts.zip
fi
echo ""
echo "Installation Finished..."
sleep 3
clear

echo "Now configuring conky for your desktop."
sleep 2
echo ""
echo "How to use this setup"
echo ""
echo "For Unity, Gnome Classic, Mate, XFCE and Others environment you have to type: ugmx"
echo "For Gnome Shell and Cinnamon environments you have to type: gnome"
sleep 2
echo ""
echo ".........................................."
echo ".........................................."
echo ""
DPATH="$HOME/.conkyrc"
TFILE="/tmp/out.tmp.$$"
echo "Which environment you are using? "
echo "ugmx <- For Unity, Gnome Classic, Mate, XFCE, and Other environments"
echo "gnome <- For Gnome Shell and Cinnamon environments"
echo ""
echo "Type: "
read ENVIR
OVERRIDE="own_window_type override"
DESKTOP="own_window_type conky"
for File in $DPATH
do
	if [ $ENVIR = "ugmx" ]; then
	{
		if [ -f $File -a -r $File ]; then
		sed "s/$DESKTOP/$OVERRIDE/g" "$File" > $TFILE && mv $TFILE "$File"
		else
   		echo "Error: Conkyrc file is not installed $File"
  		fi
	}
	elif [ $ENVIR = "gnome" ]; then
	{
		if [ -f $File -a -r $File ]; then
		sed "s/$OVERRIDE/$DESKTOP/g" "$File" > $TFILE && mv $TFILE "$File"
		else
		echo "Error: Conkyrc file is not installed $File"
  		fi
	}
	else
		echo "Invalid input!!!"
	fi
done
clear
echo "Installation Finished..."
clear


#Add conky to start up
echo "Adding conky to start up"
sleep 3
if [ -d "$HOME/.config/autostart" ]; then
	cd && wget -O .start-conky http://drive.noobslab.com/data/conky/flair/start-conky
	chmod +x $HOME/.start-conky
	wget -O start-conky.desktop http://drive.noobslab.com/data/conky/flair/start-conky.desktop
	mv -f $HOME/start-conky.desktop $HOME/.config/autostart/
else
	mkdir $HOME/.config 2> /dev/null
	mkdir $HOME/.config/autostart
	cd && wget -O .start-conky http://drive.noobslab.com/data/conky/flair/start-conky
	chmod +x $HOME/.start-conky
	wget -O start-conky.desktop http://drive.noobslab.com/data/conky/flair/start-conky.desktop
	mv -f $HOME/start-conky.desktop $HOME/.config/autostart/
fi

ACC_NAME=$(whoami)
sed -i -e "s/NoobsLab/$ACC_NAME/g" "$HOME/.config/autostart/start-conky.desktop"
echo "Successfully added to startup"
sleep 3

clear
echo "System is Updating fonts ..."
echo "System needs permissions to update fonts cache."
sleep 1
sudo fc-cache -fv
echo ""
clear
echo ".........................................."
echo ""
echo "If you made any mistake then repeat process, if not then just move on"
sleep 3
clear
echo "Logout and login back."
echo "For more visit on site - http://www.NoobsLab.com"
rm $HOME/flair-conky.sh 2> /dev/null
exit 0
