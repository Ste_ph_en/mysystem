#!/bin/bash
#This Script is written by Umair - Copyright© http://www.NoobsLab.com
#To setup conky

echo "
 +-+-+-+-+-+-+-+-+-+-+-+-+
 |N o o b s L a b . c o m|
 +-+-+-+-+-+-+-+-+-+-+-+-+
"
echo "Removing conky"
killall conky 2> /dev/null
rm -r $HOME/.conky* 2> /dev/null
echo "."
sleep 1
rm $HOME/.start-conky 2> /dev/null
echo ".."
sleep 1
rm $HOME/.config/autostart/start-conky.desktop 2> /dev/null
echo "..."
sleep 1
rm $HOME/.config/autostart/conky* 2> /dev/null
echo "...."
sleep 3
echo "For more visit on site - http://www.NoobsLab.com"
rm $HOME/uninstall-flair-conky.sh 2> /dev/null
exit 0
