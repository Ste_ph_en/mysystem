


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Adding load-path directory
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; Adding tmemes directory path
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (zenburn)))
 '(custom-safe-themes
   (quote
    ("2116ed2bb7af1ff05bef1d9caf654ae2820088c86d50c83cd8f90bf83ce0cbcc" default)))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (go-mode neotree autopair multi-term py-autopep8 flycheck jedi elpy better-defaults rainbow-delimiters expand-region shell-pop nadvice)))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
 ;; remove scroll bare and menu bar
(toggle-scroll-bar -1)
(menu-bar-mode -1)
(global-linum-mode t)  ;; enable line numbers globally
(toggle-truncate-lines -1)  ;; turn off text wrap to next line
(global-set-key (kbd "C-z") 'undo) ;; change undo
;;-----------------------------------------------------------
;; Making emacs into IDE for python
;;-----------------------------------------------------------
(elpy-enable) ;; most of the stuff like Automatic Indentation
;; syntax highlighting, checking and auto completion
(setq elpy-rpc-backend "jedi")
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; set python env to base
(require 'pyvenv)
(pyvenv-activate "/home/stephen/anaconda3/")

;; auto format python on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;; when exicuting python code uses ipython/Jupyter
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

;; Don't clutter with #files either
;;(setq auto-save-file-name-transforms
;;      `((".*" ,(expand-file-name (concat dotfiles-dir "backups")))))
;;--------------------------------------------------------------------
;; other configs
;;--------------------------------------------------------------------

;; makes matching {[("''")]}
(require 'autopair)
(autopair-global-mode) ;; enable autopair in all buffers

;;; backup/autosave
(defvar backup-dir (expand-file-name "~/.emacs.d/backup/"))
(defvar autosave-dir (expand-file-name "~/.emacs.d/autosave/"))
(setq backup-directory-alist (list (cons ".*" backup-dir)))
(setq auto-save-list-file-prefix autosave-dir)
(setq auto-save-file-name-transforms `((".*" ,autosave-dir t)))

;; emacs command in term and auto rename with multiple term
(require 'multi-term)
(setq multi-term-program "/bin/bash")
(add-hook 'multi-term-hook
      (lambda ()
        (face-remap-set-base 'comint-highlight-prompt :inherit nil)))
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'multi-term-hook 'ansi-color-for-comint-mode-on)
(multi-term)


;;This will kill the current visible buffer without confirmation unless the buffer has been modified. In this last case, you have to answer y/n.
(global-set-key [(control x) (k)] 'kill-this-buffer)

;;tricks emacs to exit while job is running without prompt by removing act jobs list
(require 'cl-lib)
(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  "Prevent annoying \"Active processes exist\" query when you quit Emacs."
  (cl-letf (((symbol-function #'process-list) (lambda ())))
    ad-do-it))

;;make emacs transparent
;;(set-frame-parameter (selected-frame) 'alpha '(85 50))
;;(add-to-list 'default-frame-alist '(alpha 85 50))
;;(add-to-list 'default-frame-alist '(fullscreen . maximized)) ;; start max
;;Load Go-specific language syntax
;;For gocode use https://github.com/mdempsky/gocode


